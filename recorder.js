const device = navigator.mediaDevices.getUserMedia({audio: true});
var items = [];

device
  .then(stream => {
    const recorder = new MediaRecorder(stream);
    recorder.ondataavailable = e => {
      items.push(e.data)
      if(recorder.state == 'inactive') {
        const blob = new Blob(items, {type: 'audio/webm'});
        console.log(blob.size * 0.001, 'KB');
        const audio = document.getElementById('audio');
        const mainAudio = document.createElement('audio');

        mainAudio.setAttribute('controls', 'controls');
        audio.appendChild(mainAudio);
        mainAudio.innerHTML = '<source src="' + URL.createObjectURL(blob) + '" type="audio/webm">'
      }
    }
    recorder.start();
    setTimeout(() => {
      recorder.stop();
    }, 1000 * 10);
  })
  .catch(err => {
    console.log('sem acesso ao microfone :(', err);
  })
